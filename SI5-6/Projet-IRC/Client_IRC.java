import java.io.*;
import java.net.*;

public class client_irc {

    public static void main(String[] args) throws Exception {

        // Serveur auquel on veut se connecter & informations de l'utilisateur
        String server = "traydent.info";
        String nick = "Florian_test";
        String login = "Florian";
        String message  = "Coucou";

        // Le canal que l'on souhaite rejoindre
        String channel = "#help";
        
        // Connexion au serveur de chat IRC
        Socket socket = new Socket(server, 6667);
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream( )));
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(socket.getInputStream( )));
        
        // Identification au serveur
        writer.write("NICK " + nick + "\r\n");
        writer.write("USER " + login + " 8 * : Java IRC \r\n");
        writer.flush( );
        
        String line = null;
        while ((line = reader.readLine( )) != null) {
            if (line.indexOf("004") >= 0) {
                // On est connecté
                break;
            }
            else if (line.indexOf("433") >= 0) {
                System.out.println("Nom déjà en cours d'utilisation.");
                return;
            }
        }
        
        // On rejoint avec le canal (ici #help)
        writer.write("JOIN " + channel + "\r\n");
        writer.flush( );

        while ((line = reader.readLine( )) != null) {
            if (line.toLowerCase( ).startsWith("PING ")) {
                writer.write("PONG " + line.substring(5) + "\r\n");
                writer.write("PRIVMSG " + channel + " :I got pinged!\r\n"+message);
                writer.flush( );
            }
            else {
                System.out.println(line);
            }
        }
    }

}