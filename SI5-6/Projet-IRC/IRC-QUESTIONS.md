##### ***MASSÉ FLORIAN & FONTAINE GUILLAUME***

---

# ***Projet Etude et mise en oeuvre du protocole IRC***

---

# Etape 1 : Etude du protocole IRC

## ***Questions :***

1. et 2. Étapes communication avec le serveur à partir de notre capture Wireshark :

- Envoi d'une requête avec la commande **CAP LS**
- Envoi d'une requête avec la commande **NICK** pour demander le nom d'utilisateur
- Réponse du serveur avec la commande **NOTICE** pour afficher une réponse/un message 
(exemple : "Response: :moulinsart.traydent.info NOTICE * :"***" Looking up your hostname...")
- Envoi de requêtes et de réponses **CAP**
- Fin du CAP **("Request: CAP END")**
- Message de bienvenue sur le chat IRC du BTS : "Response: :moulinsart.traydent.info 001 Flo :Welcome to the BTS Internet Relay Chat Network Flo "
- Envoi d'une requête avec la commande **JOIN** au serveur avec **#help**
- Réponse pour cette demande
-  Envoi d'une requête avec la commande **PING** le client ping le serveur
- Envoi d'une requête avec la commande **PRIVMSG** qui est "private message", cela signifie que quelqu'un a envoyé un message, ici le message était "Bijour".

3.Le client ping à intervalles réguliers le serveur afin de tester la connexion entre ces derniers, et voir si leurs connexion à internet est toujours active.

---

# Etape 2 : Utilisation d’un client Telnet pour communiquer avec un serveur de chat IRC

Commandes à utiliser dans telnet pour communiquer avec le serveur traydent.info : 

**o** traydent.info 6667 (double entrée)
**NICK** Flo
**USER** Flo Flo traydent.info :realname

**JOIN** #help

**PRIVMSG** #help "message"**
**QUIT** (pour quitter)