# TP PROTOCOLE ACTIVITÉ 3
###### Massé Florian

---

* ***HTTP*** (HyperText Transfer Protocol) : C'est un protocole de communication client-serveur. Il est utilisé pour le World Wide Web, c'est à dire que c'est le web en générale, il est utilisé pour tout type d'action. L'"HTTPS" signifie que la page est sécurisée et vérifiée.  Il 
permet la lecture sur un navigateur (le  client) de pages Web localisées par  leur  adresse  URL  sur un serveur Web. 

~~~
Commandes :

GET : Requête de la ressource située à l'URL spécifiée
HEAD : Requête de l'en-tête de la ressource située à l'URL spécifiée
POST : Envoi de données au programme situé à l'URL spécifiée
PUT : Envoi de données à l'URL spécifiée
DELETE : Suppression de la ressource située à l'URL spécifiée

~~~

Clients qui parlent l'http :  Apache, Zeus Web Server, nginx, lighttpd, Cherokee, Hiawatha Webserver

Sources : 
https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol
http://www.commentcamarche.net/contents/520-le-protocole-http

---

* ***IRC*** : Internet Relay Chat ou IRC (en français, « discussion relayée par Internet ») est un protocole de communication textuelle sur Internet. Ce protocole permet de discuter instantanément sur un réseau en utlisant des canaux (canal). Il existe différents logiciels pour communiquer en IRC tels que HexChat.

Clients qui parlent l'IRC :  IRCnet, DALnet, EFnet, Undernet

***On distingue deux types de clients :***

* Les clients utilisateurs qui fournissent généralement une interface basée sur le texte et qui est utilisé pour communiquer interactivement via IRC.
* Les clients de service qui sont généralement des bots utilisés pour fournir une fonctionnalité supplémentaire sur le réseau.
    Ces fonctionnalités ne sont généralement pas reliées directement au protocole IRC en lui-même.

Chaque client est ensuite connecté à un seul et unique serveur et est ainsi en liaison avec tous les autres clients et serveurs. Chaque client est identifié par un pseudonyme (nick) unique sur le réseau, composé de 9 caractères au plus. De plus, chaque client doit fournir aux serveurs un nom et un domaine de connexion (host). Chaque serveur est tenu de savoir à quel serveur est connecté directement un client. Pour communiquer entre eux, les clients ont deux possibilités : soit désigner explicitement les destinataires du message, soit joindre un chan (salle de discussion) et envoyer les messages à celui-ci (et par propagation à tous les clients qui y sont présents). 

Sources :
http://mathieu-lemoine.developpez.com/tutoriels/irc/protocole/?page=generalites
https://fr.wikipedia.org/wiki/Internet_Relay_Chat


---

* ***FTP*** : File Transfer Protocol est un protocole de communication destiné au partage de fichiers sur un réseau TCP/IP. Il permet, depuis un ordinateur, de copier des fichiers vers un autre ordinateur du réseau, ou encore de supprimer ou de modifier des fichiers sur cet ordinateur. 
Il existe par exemple le logiciel FileZilla qui permet de transferer des fichier d'un ordinateur local à un serveur et inversement.

Le protocole FTP définit la façon selon laquelle des données doivent être transférées sur un réseau TCP/IP.

Le protocole FTP a pour objectifs de :

* permettre un partage de fichiers entre machines distantes
* permettre une indépendance aux systèmes de fichiers des machines clientes et serveur
* permettre de transférer des données de manière efficace

Autres clients : GNU inetutils : paquet logiciel GNU contenant un client FTP en ligne de commande, ftp (en ligne de commande sous Unix/Linux/Windows)

Sources : 
http://www.commentcamarche.net/contents/519-le-protocole-ftp-file-transfer-protocol
https://fr.wikipedia.org/wiki/File_Transfer_Protocol

---

* ***SMTP*** : Simple Mail Transfer Protocol est un protocole de communication utilisé pour transférer le courrier électronique vers les serveurs de messagerie électronique.

~~~

Brève explication du fonctionnement du protocole SMTP :

Pour envoyer un mail, vous utilisez un M.U.A. (Mail User Agent), il est en général de 2 types :

- un webmail (lorsque vous vous connectez en ligne)
- un client de messagerie (logiciel installé sur votre ordinateur tel que Outlook, ThunderBird, application de Smartphone, …)

Au moment ou vous envoyez votre mail, votre MUA :

- va convertir votre mail au format texte (même les pièces jointes seront converties en texte)
- va se connecter au serveur SMTP qui est paramétré et envoyer le texte.

Ensuite, le serveur SMTP va envoyer le mail vers le destinataire. Si le message doit passer entre différents serveurs, c’est toujours le protocole SMTP qui sera utilisé pour envoyer le message de serveurs en serveurs, les serveurs utilisent alors des relais SMTP.

Le mail va ainsi voyager de serveurs en serveurs, jusqu’au dépôt sur le serveur de boites postales du destinataire.

~~~

Sources : 
http://www.culture-informatique.net/cest-quoi-un-serveur-smtp/
https://fr.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol

---

