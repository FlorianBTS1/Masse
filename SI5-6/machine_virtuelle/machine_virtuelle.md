# Installer un OS sur une machine virtuelle (VirtualBox)

***Par Massé Florian***

Je vais vous expliquer comment installer l'OS DEBIAN sur le logiciel VirtualBox mais vous pouvez si vous le souhaitez mettre un autre OS.

## I. Créer sa machine Virtuelle

- Avant de commencer l'explication, vous allez devoir installer VirtualBox en le téléchargant sur https://www.virtualbox.org/
- Une fois cela fait vous allez devoir créer une nouvelle machine virtuelle en cliquant au même endroit que moi sur l'image (ci-dessous)

![m1](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m1.png)

- Il faut ensuite mettre le nom que nous souhaitons donner à notre machine virtuelle sur VirtualBox, j'aurais donc mis **"DebianExplication"**, vous devez ensuite choisir le type d'OS ainsi que la version de l'OS que vous allez virtualiser par la suite, j'ai choisis **"Linux"** pour le Type et **"Debian (64-bit)"** pour la Version

     ![m2](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m2.png)

- Un fois cela fait faire "Suivant", il faut ensuite choisir la **Taille de la mémoire**, c'est-à-dire la RAM que nous allons accorder à notre machine virtuelle, j'aurais mis la quantitée recommandée qui est de **1024 MO**.

     ![m3](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m3.png)

- Il faut faire "Suivant", et choisir l'endroit où le disque dur de notre machine virtuelle sera, je choisi "Créer un disque dur virtuel maintenant" et je fais "Créer"

     ![m4](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m4.png)
     
- Lorsque j'ai fais cela, il faut choisir le type de disque dur et il faut prendre le **"VMDK (Disque Virtual Machine)"** et faire "Suivant"

     ![m5](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m5.png)
     
- Quand j'ai choisit le type de fichier de disque dur, je choisis **Dynamiquement alloué**, mais si je veux je peux choisir l'autre option, c'est selon notre machine.

     ![m6](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m6.png)
     
- Je dois maintenant choisir la taille que maximale que je souhaites attribuer à ma machine virtuelle par rapport à l'espace du disque dur de ma machine, je choisis donc **8Go** car c'est ce qui est recommandé, mais vous pouvez mettre plus d'espace si vous le souhaitez, et je peux également si je le souhaite changer le chemin de l'emplacement du fichier

     ![m7](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m7.png)
     
- J'ai maintenant créé ma machine virtuelle ! Il va falloir maintenant mettre l'image ISO de mon OS que je souhaites mettre dans ma machine virtuelle ! Je vais vous présenter ça dans une seconde partie

     ![m8](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m8.png)
     
## II. Mettre l'ISO de l'OS dans la machine virtuelle

- Pour mettre l'ISO de l'OS dans notre machine virtuelle, il va falloir faire un clic droit sur notre machine virtuelle et cliquer sur **"Configuration"**

     ![m8](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m8.png)
     
     ![m9](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m9.png)
     
- Une fois dans configuration, il faut aller dans ***Stockage***, puis dans le disque ***Vide***

     ![m10](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m10.png)
    
- Il faut maintenant cliquer sur le petit disque en haut à droite, et choisir le disque optique virtuel, c'est-à-dire l'image ISO de notre OS, ici pour moi c'est l'ISO de Debian, que vous pouvez **télécharger gratuitement** ici : https://www.debian.org 

     ![m11](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m11.png)

- Il faut faire Ok et l'OS est maintenant prêt à être installer sur notre machine virtuelle, nous pouvons maintenant cliquer sur la flèche verte pour démarrer la machine virtuelle

     ![m12](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m12.png)
   
- Lorsque nous avons fait tout ça, la machine virtuelle est lancée, donc je vais vous expliquer comment installer Debian, mais pas un autre OS pour cette explication.
	Il faut donc choisir ***64 bit graphical install***

     ![m13](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m13.png)
     
- Choisissez la **langue** que vous souhaitez, je choisis donc le français

     ![m14](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m14.png)
     
- Par la suite je continue en choisissant mon **pays**, la **langue** pour le clavier, je fais continuer

- Il faut ensuite mettre le nom du système et faire continuer

     ![m15](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m15.png)
     
- Maintenant vous devez mettre un **mot de passe** pour le système et vous devez ensuite le confirmer

     ![m16](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m16.png)
     
- Vous devez choisir un nom d'**utilisateur**

     ![m17](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m17.png)
     
- Mettre un **mot de passe** pour l'**utilisateur**

     ![m18](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m18.png)
     
- Une fois le mot de passe utilisateur mis, nous allons configurer les ***partitions du disque dur***

     ![m19](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m19.png)
     
- Vous pouvez réaliser les mêmes étapes que moi si vous le souhaitez, je choisis donc le disque entier et je choisis ensuite le disque que j'ai créé au début

     ![m20](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m20.png)
     
     ![m21](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m21.png)
     
     ![m22](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m22.png)
     
- Il faut maintenant choisir **"Oui"** pour appliquer le partitionnement

     ![m23](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m23.png)
     
- Nous allons maintenant configuer les paquets, vous devez chosir votre pays, ensuite vous faites ***Continuer***

     ![m24](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m24.png)
     
     ![m25](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m25.png)
     
     ![m26](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m26.png)
     
     ![m27](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m27.png)
     
- Vous allez maintenant arriver sur une fenêtre avec les différents **logiciels** que vous pouvez installer, j'installe donc ceux dont j'aurais besoin, vous prenez ceux dont vous aurez besoin également, car chaque logiciel prend de la place sur notre disque dur virtuel

     ![m28](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m28.png)
     
- Pour l'installation du **GRUB**, vous devez faire **Oui**

     ![m29](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m29.png)
     
- Choisissez la même chose que moi pour l'installation du **GRUB**

     ![m30](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m30.png)
 
- Quelques minutes plus tard, votre machine virtuelle est maintenant **installée**, bravo ! 
     
     ![m31](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m31.png)
     
- Pour continuer, vous lancez votre machine virtuelle une fois qu'elle est bien installé avec l'OS dessus (Debian).  Vous faites la touche **Entrée** lorsque vous avez le même écran que moi

     ![m32](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m32.png)
     
- Vous accedez à vôtre session

     ![m33](C:\Users\Florianpc\Desktop\Ordi\BTS1\SI56\machine_virtuelle/m33.png)

     
 