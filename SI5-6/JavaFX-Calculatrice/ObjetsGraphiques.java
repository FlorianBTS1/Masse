import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ObjetsGraphiques extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Première Application JavaFX");
		GridPane grid = new GridPane(); //Les 'GridPane' sont des options d'affichage
		//grid.setGridLinesVisible(true);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		grid.setStyle("; -fx-background-color: CORNFLOWERBLUE");
		
		GridPane grid1 = new GridPane(); //Les 'GridPane' sont des options d'affichage
		//grid1.setGridLinesVisible(true);
		grid1.setAlignment(Pos.CENTER);
		grid1.setHgap(10);
		grid1.setVgap(10);
		grid1.setPadding(new Insets(0, 10, 0, 10));

		GridPane grid2 = new GridPane(); //Les 'GridPane' sont des options d'affichage
		//grid2.setGridLinesVisible(true);
		grid2.setAlignment(Pos.CENTER);
		grid2.setHgap(10);
		grid2.setVgap(10);
		grid2.setPadding(new Insets(0, 10, 0, 10));
		
		GridPane grid3 = new GridPane(); //Les 'GridPane' sont des options d'affichage
		//grid3.setGridLinesVisible(true);
		grid3.setAlignment(Pos.CENTER);
		grid3.setHgap(10);
		grid3.setVgap(10);
		grid3.setPadding(new Insets(25, 25, 25, 25));
		
		Text scenetitle = new Text("Effectuez votre calcul :"); // Texte pour s'identifier
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);

		Label userName = new Label("1er Nombre :"); // Texte devant la ligne du nom d'utilisateur
		grid2.add(userName, 0, 1);

		TextField userTextField = new TextField();
		grid2.add(userTextField, 1, 1);

		Label pw = new Label("2ème Nombre :");// Texte devant la ligne du mot de passe
		grid2.add(pw, 0, 2);

		TextField pwBox = new TextField();
		grid2.add(pwBox, 1, 2);

		Button btn = new Button("Multiplier");
		btn.setTextFill(Color.web("#000000"));
        btn.setStyle("-fx-background-color: LIGHTGREEN");
		HBox hbBtn = new HBox(15);
		hbBtn.setAlignment(Pos.BOTTOM_LEFT);
		hbBtn.setStyle("-fx-border-color: BLACK");
		hbBtn.getChildren().add(btn);
		grid1.add(hbBtn, 0, 0);

		Button btn2 = new Button("Diviser");
		btn2.setTextFill(Color.web("#000000"));
        btn2.setStyle("-fx-background-color: LIGHTGREEN");
		HBox hbBtn2 = new HBox(15);
		hbBtn2.setAlignment(Pos.BOTTOM_LEFT);
		hbBtn2.setStyle("-fx-border-color: BLACK");
		hbBtn2.getChildren().add(btn2);
		grid1.add(hbBtn2, 1, 0);

		Button btn3 = new Button("Soustraire");
		btn3.setTextFill(Color.web("#000000"));
        btn3.setStyle("-fx-background-color: LIGHTGREEN");
		HBox hbBtn3 = new HBox(10);
		hbBtn3.setAlignment(Pos.BOTTOM_LEFT);
		hbBtn3.setStyle("-fx-border-color: BLACK");
		hbBtn3.getChildren().add(btn3);
		grid1.add(hbBtn3, 2, 0);

		Button btn4 = new Button("Additionner");
		btn4.setTextFill(Color.web("#000000"));
        btn4.setStyle("-fx-background-color: LIGHTGREEN");
		HBox hbBtn4 = new HBox(10);
		hbBtn4.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn4.setStyle("-fx-border-color: BLACK");
		hbBtn4.getChildren().add(btn4);
		grid1.add(hbBtn4, 3, 0);

		//final Text actiontarget = new Text();
		//grid.add(actiontarget, 1, 3);
		
		//btn.setOnAction(new EventHandler<ActionEvent>() {

			//@Override
			//public void handle(ActionEvent e) {
				//actiontarget.setFill(Color.BLACK);
				//actiontarget.setText("Calcul en cours..."); // Message indiquant que la connexion est en cours d'établissement
			//}
		//});

		grid.add(grid1, 1, 2);
		grid.add(grid2, 1, 1);
		grid.add(grid3, 1, 3);
		
		Label res = new Label ("55");
		res.setFont(Font.font("Monospace", FontWeight.NORMAL,20));
		res.setTextFill(Color.RED);
		BorderStroke res1 = new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(3), BorderStroke.MEDIUM);
		Border bord2 = new Border(res1);
		res.setBorder(bord2);
		grid3.add(res, 0, 0);
		
		Scene scene = new Scene(grid, 500, 300);
		primaryStage.setScene(scene);
		primaryStage.show();


	}
}
