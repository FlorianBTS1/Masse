import java.util.Scanner;
public class Exercice3_Boucle{

	public static void main(String[]args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Saisissez un entier positif");
		int a = sc.nextInt();
		while (a<0){
			System.out.println("Faux, il faut recommencer");
			a = sc.nextInt();
			
		}
		System.out.println("Vous avez saisi un nombre positif");
	}

}
