import java.util.Scanner;
public class Exercice4{
	public static void main(String[]args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Saisir un nombre compris entre 1 et 3.");
		int a = sc.nextInt();
		while (a >3 || a <1){
			System.out.println("Le nombre " +a+ " n'est pas compris entre 1 et 3. Il faut recommencer !");
			a=sc.nextInt();
		}
		System.out.println("Bravo! Le nombre " +a+ " est bien compris entre 1 et 3.");
	}

}
