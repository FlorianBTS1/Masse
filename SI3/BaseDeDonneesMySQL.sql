-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2016 at 01:52 PM
-- Server version: 5.5.49-0+deb8u1
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `massef`
--

-- --------------------------------------------------------

--
-- Table structure for table `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal`
--

INSERT INTO `animal` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'Pomponnette'),
(3, 'Jerry'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Table structure for table `animal2`
--

CREATE TABLE IF NOT EXISTS `animal2` (
`numAnimal2` int(11) NOT NULL,
  `nom2` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal2`
--

INSERT INTO `animal2` (`numAnimal2`, `nom2`) VALUES
(1, 'Pompom'),
(2, 'Pomponnette'),
(3, 'Jerry'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Table structure for table `CATALOGUE`
--

CREATE TABLE IF NOT EXISTS `CATALOGUE` (
  `Num coll` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`idCli` int(11) NOT NULL,
  `nomCli` varchar(20) NOT NULL,
  `prenomCli` varchar(20) NOT NULL,
  `adresse` varchar(20) NOT NULL,
  `dateNaiss` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`idCli`, `nomCli`, `prenomCli`, `adresse`, `dateNaiss`) VALUES
(1, 'Harry', 'Charles', '12 rue du prÃ©', '1987-11-12'),
(2, 'Dupond', 'John', '13 rue du parc', '1987-11-26'),
(3, 'Dupo', 'Winnie', '19 rue du parc', '2004-02-10'),
(4, 'Dupo', 'Jennifer', '12 rue du parc', '2002-05-09'),
(5, 'Dodo', 'Dorianne', '28 rue du pommier', '2011-11-16'),
(6, 'Binouf', 'Baptiste', '19 rue du chene', '2011-11-18'),
(7, 'Boria', 'Barry', '29 rue du pommier', '2011-11-23'),
(8, 'Bibi', 'Foque', '29 rue du mistral', '2011-11-18'),
(9, 'Wang', 'Goth', '18 rue du prunus', '2011-10-12'),
(10, 'frada', 'Frederic', '27 rue du chene', '2011-10-13'),
(11, 'Faudiere', 'frederic', '19 rue du prunus', '2011-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
`numeroC` int(11) NOT NULL,
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `PrenomVeterinaire` int(11) NOT NULL,
  `NomVeterinaire` int(11) NOT NULL,
  `NumTelephoneVeterinaire` int(11) NOT NULL,
  `PrenomMedecin` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`numeroC`, `numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `PrenomVeterinaire`, `NomVeterinaire`, `NumTelephoneVeterinaire`, `PrenomMedecin`) VALUES
(6, 1, 1, '2013-09-15', 'problème oculaire', 0, 0, 0, 0),
(7, 1, 2, '2013-09-20', 'vaccin', 0, 0, 0, 0),
(8, 2, 3, '2013-09-03', 'plaie à la patte', 0, 0, 0, 0),
(9, 3, 4, '2013-09-10', 'trouble du sommeil', 0, 0, 0, 0),
(10, 4, 5, '2013-09-01', 'problèmes comporteme', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `consultation2`
--

CREATE TABLE IF NOT EXISTS `consultation2` (
`numeroC2` int(11) NOT NULL,
  `numeroProprio2` int(11) NOT NULL,
  `numeroAnimal2` int(11) NOT NULL,
  `dateConsultation2` date NOT NULL,
  `raison2` varchar(20) NOT NULL,
  `prenomMedecin2` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contrat`
--

CREATE TABLE IF NOT EXISTS `contrat` (
`idContrat` int(11) NOT NULL,
  `idCli` int(11) NOT NULL,
  `idLogement` int(11) NOT NULL,
  `date` date NOT NULL,
  `montant` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contrat`
--

INSERT INTO `contrat` (`idContrat`, `idCli`, `idLogement`, `date`, `montant`) VALUES
(1, 6, 4, '2011-12-30', 200),
(2, 3, 6, '2011-09-14', 250),
(3, 4, 6, '2011-11-17', 100),
(4, 11, 7, '2011-11-21', 120),
(5, 6, 3, '2011-11-05', 123),
(6, 8, 4, '2011-11-21', 206),
(7, 10, 5, '2011-11-19', 209),
(8, 1, 7, '2011-11-28', 301),
(9, 1, 7, '2011-11-11', 400),
(10, 10, 2, '2011-11-24', 800);

-- --------------------------------------------------------

--
-- Table structure for table `logement`
--

CREATE TABLE IF NOT EXISTS `logement` (
`idLogement` int(11) NOT NULL,
  `nbPiece` int(11) NOT NULL,
  `adresse` varchar(20) NOT NULL,
  `valeurEstimative` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logement`
--

INSERT INTO `logement` (`idLogement`, `nbPiece`, `adresse`, `valeurEstimative`) VALUES
(1, 5, '29 rue du parc', 200000),
(2, 6, '17 rue du pommier', 400000),
(3, 24, '19 rue du chene', 1000000),
(4, 2, '17 rue du havre', 100000),
(5, 3, '15 rue du mistral', 130000),
(6, 8, '11 rue du chateigner', 600000),
(7, 5, '6 rue du chene', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `Medecin`
--

CREATE TABLE IF NOT EXISTS `Medecin` (
`numMedecin` int(11) NOT NULL,
  `nomMedecin` int(11) NOT NULL,
  `prenomMedecin` int(11) NOT NULL,
  `telMedecin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proprietaire`
--

CREATE TABLE IF NOT EXISTS `proprietaire` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proprietaire`
--

INSERT INTO `proprietaire` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Aimée', 'Madeleine'),
(3, 'Brown', 'Charly'),
(4, 'Gargamel', 'Jules');

-- --------------------------------------------------------

--
-- Table structure for table `proprietaire2`
--

CREATE TABLE IF NOT EXISTS `proprietaire2` (
`numProprio2` int(11) NOT NULL,
  `nomProprio2` varchar(20) NOT NULL,
  `prenomProprio2` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animal`
--
ALTER TABLE `animal`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Indexes for table `animal2`
--
ALTER TABLE `animal2`
 ADD PRIMARY KEY (`numAnimal2`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`idCli`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
 ADD PRIMARY KEY (`numeroC`), ADD KEY `numeroAnimal` (`numeroAnimal`), ADD KEY `numeroProprio` (`numeroProprio`);

--
-- Indexes for table `consultation2`
--
ALTER TABLE `consultation2`
 ADD PRIMARY KEY (`numeroC2`), ADD KEY `numeroAnimal2` (`numeroAnimal2`), ADD KEY `numeroProprio2` (`numeroProprio2`);

--
-- Indexes for table `contrat`
--
ALTER TABLE `contrat`
 ADD PRIMARY KEY (`idContrat`), ADD KEY `idCli` (`idCli`), ADD KEY `idLogement` (`idLogement`);

--
-- Indexes for table `logement`
--
ALTER TABLE `logement`
 ADD PRIMARY KEY (`idLogement`);

--
-- Indexes for table `Medecin`
--
ALTER TABLE `Medecin`
 ADD PRIMARY KEY (`numMedecin`), ADD KEY `nomMedecin` (`nomMedecin`), ADD KEY `prenomMedecin` (`prenomMedecin`);

--
-- Indexes for table `proprietaire`
--
ALTER TABLE `proprietaire`
 ADD PRIMARY KEY (`numProprio`);

--
-- Indexes for table `proprietaire2`
--
ALTER TABLE `proprietaire2`
 ADD PRIMARY KEY (`numProprio2`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal`
--
ALTER TABLE `animal`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `animal2`
--
ALTER TABLE `animal2`
MODIFY `numAnimal2` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `idCli` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
MODIFY `numeroC` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `consultation2`
--
ALTER TABLE `consultation2`
MODIFY `numeroC2` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contrat`
--
ALTER TABLE `contrat`
MODIFY `idContrat` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `logement`
--
ALTER TABLE `logement`
MODIFY `idLogement` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Medecin`
--
ALTER TABLE `Medecin`
MODIFY `numMedecin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `proprietaire`
--
ALTER TABLE `proprietaire`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `proprietaire2`
--
ALTER TABLE `proprietaire2`
MODIFY `numProprio2` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contrat`
--
ALTER TABLE `contrat`
ADD CONSTRAINT `contrat_ibfk_1` FOREIGN KEY (`idLogement`) REFERENCES `logement` (`idLogement`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
