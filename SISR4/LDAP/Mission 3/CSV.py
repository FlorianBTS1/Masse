#!/usr/bin/python3

# read_csv3.py

import csv

f = open('test.csv', 'r')

with f:
    
#Délimiteur, il faut spécifier quel caractère nous délimitons ici dans notre fichier .csv, nous avons des ";" donc il faut le préciser.
    reader = csv.DictReader(f, delimiter=";")

    #nombre de ligne/colones avant le for
    n = 0
    
    for row in reader:
        #affiche la ligne/colone "Prénom" et "Age".
        print(row['Prénom'], row['Age'])
        n+=1 #on ajoute +1
print (n) #affiche le nombre de lignes du fichier !!

        


