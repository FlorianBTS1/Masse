# *Compte rendu – LDAP : Mission 1*
Massé Florian Le 10/04/18

---

_Dans ce compte rendu, je vais expliquer comment utiliser_ **Apache Directory.**

---

**Sommaire :**

I. _LDAP – Définition_
II. _Installation d'Apache Directory Studio_
III. _Configuration d'Apache Directory Studio_
IV. _Utilisation d'Apache Directory Studio_

---

## I. ***LDAP – Définition***

_LDAP (Lightweight Directory Access Protocol, traduisez Protocole d'accès aux annuaires léger) est un protocole standard permettant de gérer des annuaires, c'est-à-dire d'accéder à des bases d'informations sur les utilisateurs d'un réseau par l'intermédiaire de protocoles TCP/IP._

---

## II. ***Installation d'Apache Directory Studio***

_Pour commencer l'installation, il faudra des prérequis :_

- **Apache Directory Studio** _installé sur votre poste, le téléchargement est disponible ici :_ [_http://directory.apache.org/studio/_](http://directory.apache.org/studio/)
- **JDK** _(Java Development Kit) installé ainsi que_ **Java** _installés sur votre poste, le téléchargement est disponible ici :_ [_http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html_](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

---

## III. ***Installation d'Apache Directory Studio***

_Une fois tous ces logiciels nécessaires à l'installation installés, vous allez lancer Apache Directory Studio._

_Il faut se diriger en bas à gauche dans connexions, puis clic droit « Nouvelle connexion… »_

![ldap1](ldap1.png)

_Ensuite nous devons configurer notre nouvelle connexion comme suit :_

![ldap1](ldap2.png)

_Il faut ensuite faire suivant, puis choisir « Pas d'authentification » dans Méthode d'authentification, puis faire Terminer._

![ldap1](ldap3.png)

_Une fois cette étape faite, il faut aller en bas à gauche choisir l'onglet_ **LDAP Servers** _. Dans cet onglet faire clic droit et Nouveau > Nouveau serveur_

![ldap1](ldap4.png)

_Il faut ensuite choisir le serveur ApacheDS 2.0.0 et faire Terminer._

![ldap1](ldap5.png)



_Nous pouvons ensuite démarrer le serveur en faisant clic droit dessus puis « Démarrer »._

![ldap1](ldap6.png)



_Nous retournons maintenant dans l'onglet connexions et nous allons nous connecter au localhost, la base de données._

![ldap1](ldap7.png)



_Pour se connecter au localhost, il suffit de double-cliquer dessus._

![ldap1](ldap8.png)

## IV. ***Utilisation d'Apache Directory Studio***

_Tout d'abord, il faut commencer par créer une nouvelle entrée_
_La racine sio-hautil.eu est le nom de domaine qui est représenté par l'objet "domain"_

![ldap1](ldap9.png)

_Pour ajouter une nouvelle entrée, il faut faire clic droit sur le domaine ensuite nouveau puis nouvelle entrée_

![ldap1](ldap10.png)

_Aller dans méthode de création d'entrée puis faire "Utiliser une entrée existante comme modèle" et écrire ou=nom que l'on veut_

![ldap1](ldap11.png)

_Pour ajouter le nom d'une personne, il faut faire clic droit sur une entrée d'une catégorie, puis faire nouvelle entrée et aller mettre l'objet "organizationalPerson", faire suivant puis dans "RDN" mettre "commonName"_

![ldap1](ldap12.png)

![ldap1](ldap13.png)

_Voici les différentes recherches que j'ai réalisé pour la mission 1, il faut aller en haut et faire "Rechercher" :_

![ldap1](ldap14.png)

![ldap1](ldap15.png)

![ldap1](ldap16.png)

![ldap1](ldap17.png)
